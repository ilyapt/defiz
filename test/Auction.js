const ethSigUtil = require('eth-sig-util');
const Wallet = require('ethereumjs-wallet').default;
const ethUtil = require("ethereumjs-util");
const { expect } = require('chai');

const TokenMock = artifacts.require('TokenMock');
const Auction = artifacts.require('Auction');
const LimitOrderProtocol = artifacts.require('LimitOrderProtocol');

const { Order, buildOrderData } = require('./helpers/orderUtils');
const { cutLastArg } = require('./helpers/utils');

contract('Auction', async function ([_, wallet]) {
    const privatekey = '59c6995e998f97a5a0044966f0945389dc9e86dae88c7a8412f4603b6b78690d';
    const account = Wallet.fromPrivateKey(Buffer.from(privatekey, 'hex'));

    const zeroAddress = '0x0000000000000000000000000000000000000000';

    function buildOrder (o, exchange, makerAsset, takerAsset, makerAmount, takerAmount, taker = zeroAddress, predicate = '0x', permit = '0x', interaction = '0x') {
        return buildOrderWithSalt(o, exchange, '1', makerAsset, takerAsset, makerAmount, takerAmount, taker, predicate, permit, interaction);
    }

    function buildOrderWithSalt (o, exchange, salt, makerAsset, takerAsset, makerAmount, takerAmount, taker = zeroAddress, predicate = '0x', permit = '0x', interaction = '0x') {

        let data = o.contract.methods.price(takerAmount, 100000, 10, '0x').encodeABI();
        let price = cutLastArg(exchange.contract.methods.arbitraryStaticCall(o.address, data).encodeABI());

        let msg = web3.eth.abi.encodeParameters(['address', 'address', 'uint256'], [makerAsset.address, takerAsset.address, 99960]);
        let hashed = ethUtil.keccak256(ethUtil.toBuffer(msg))
        let sig = ethUtil.ecsign(hashed, account.getPrivateKey());
        let sign = ethUtil.bufferToHex(ethSigUtil.concatSig(sig.v, sig.r, sig.s));

        let intData = web3.eth.abi.encodeParameters(['uint256', 'uint', 'uint', 'bytes'], [100000, 10, 20, sign]);

        let itr = o.contract.methods.notifyFillOrder(makerAsset.address, takerAsset.address, makerAmount, takerAmount, intData).encodeABI();

        return {
            salt: salt,
            makerAsset: makerAsset.address,
            takerAsset: takerAsset.address,
            makerAssetData: makerAsset.contract.methods.transferFrom(o.address, taker, makerAmount).encodeABI(),
            takerAssetData: takerAsset.contract.methods.transferFrom(taker, wallet, takerAmount).encodeABI(),
            getMakerAmount: price,
            getTakerAmount: cutLastArg(exchange.contract.methods.getTakerAmount(makerAmount, takerAmount, 0).encodeABI()),
            predicate: predicate,
            permit: permit,
            interaction: itr,
        };
    }

    beforeEach(async function () {
        this.swap = await LimitOrderProtocol.new();
        this.auction = await Auction.new(this.swap.address);

        this.dai = await TokenMock.new('DAI', 'DAI');
        this.weth = await TokenMock.new('WETH', 'WETH');


        // We get the chain id from the contract because Ganache (used for coverage) does not return the same chain id
        // from within the EVM as from the JSON RPC interface.
        // See https://github.com/trufflesuite/ganache-core/issues/515
        this.chainId = await this.dai.getChainId();

        await this.dai.mint(account.getAddressString(), '1000000');
        await this.weth.mint(account.getAddressString(), '1000000');
        await this.dai.mint(_, '1000000');
        await this.weth.mint(_, '1000000');
        await this.dai.mint(this.auction.address, '1000000');

        await this.dai.approve(this.swap.address, '1000000');
        await this.weth.approve(this.swap.address, '1000000');
        await this.dai.approve(this.auction.address, '1000000', { from: account.getAddressString() });
        await this.weth.approve(this.auction.address, '1000000', { from: account.getAddressString() });

    });

    describe('simple auction order', async function () {

        it('fill order', async function () {
            const order = buildOrder(this.auction, this.swap, this.dai, this.weth, 100000, 100000);
            const data = buildOrderData(this.chainId, this.swap.address, order);

            let orderSign = '0x';
            // orderSign = web3.eth.abi.encodeParameters([Order], [order]);

            this.swap.fillOrder(order, orderSign, 0, 100000, 99960);

            await new Promise(r => setTimeout(r, 1000));

            expect((await this.dai.balanceOf(wallet)).toString()).to.be.equal('900040');
            expect((await this.weth.balanceOf(wallet)).toString()).to.be.equal('1100000');

            expect((await this.dai.balanceOf(_)).toString()).to.be.equal('1099960');
            expect((await this.weth.balanceOf(_)).toString()).to.be.equal('900000');
        });
    });
});

// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "hardhat/console.sol";
import "./libraries/SilentECDSA.sol";


contract Auction {
    address _inch;
    mapping (bytes32 => bool) _filled;

    struct Order {
        uint256 salt;
        address makerAsset;
        address takerAsset;
        bytes makerAssetData; // (transferFrom.selector, signer, ______, makerAmount, ...)
        bytes takerAssetData; // (transferFrom.selector, sender, signer, takerAmount, ...)
        bytes getMakerAmount; // this.staticcall(abi.encodePacked(bytes, swapTakerAmount)) => (swapMakerAmount)
        bytes getTakerAmount; // this.staticcall(abi.encodePacked(bytes, swapMakerAmount)) => (swapTakerAmount)
        bytes predicate;      // this.staticcall(bytes) => (bool)
        bytes permit;         // On first fill: permit.1.call(abi.encodePacked(permit.selector, permit.2))
        bytes interaction;
    }

    bytes32 constant public LIMIT_ORDER_TYPEHASH = keccak256(
        "Order(uint256 salt,address makerAsset,address takerAsset,bytes makerAssetData,bytes takerAssetData,bytes getMakerAmount,bytes getTakerAmount,bytes predicate,bytes permit,bytes interaction)"
    );

    constructor(address inch) {
        _inch = inch;
    }

    function isValidSignature(bytes32 hashed, bytes memory signature) external returns (bytes4 magicValue) {
        require(msg.sender == _inch);
        Order memory order;
        // (order) = abi.decode(signature, (Order));
        // check _hash(order) == hashed
        // check order.makerAssetData receiver is address(this)
        return 0x1626ba7e;
    }

    function price(uint256 amount, uint256 startAmount, uint startBlock, bytes memory data) external view returns(uint256) {
        return _calc(amount, startAmount, startBlock);
    }

    function _calc(uint256 takerAmount, uint256 startAmount, uint startBlock) private view returns(uint256) {
        // height fixed for test for check amounts after swap
        uint height = 14;
        // height = block.number
        require(startBlock < height, "start block should be less then currect");

        // in this PoC decreasing fixed as 10 gwei per block
        uint perBlock = 10; // gwei
        uint256 calculatedAmount = startAmount - perBlock * ( height - startBlock );
        
        return calculatedAmount;
    }

    function notifyFillOrder(address makerAsset, address takerAsset, uint256 makingAmount, uint256 takingAmount, bytes calldata interaction) external {
        require(msg.sender == _inch);
        
        bytes memory sig;
        uint256 startPrice;
        uint startBlock;
        uint finalBlock;
        (startPrice, startBlock, finalBlock, sig) = abi.decode(interaction[196:], (uint256, uint, uint, bytes));
        
        // checking signature and getting signer address
        bytes32 hashed = keccak256(abi.encode(makerAsset, takerAsset, makingAmount));
        address addr = SilentECDSA.recover(hashed, sig);

        // height fixed for test for check amounts after swap
        //  should be block.number
        require(finalBlock > 14, "final block should be more then currect");

        require(_filled[hashed] == false, "each auction order can be filled once");
        _filled[hashed] = true;

        ERC20 token = ERC20(makerAsset);

        // recalculate sending amount and check its equal to makingAmount
        uint256 permittedAmount = _calc(takingAmount, startPrice, startBlock);
        require(permittedAmount == makingAmount, "recalculated amount isn't equal to makingAmount");

        // firstly transferring funds from maker to auction contract then approve them to 1inch contract
        token.transferFrom(addr, address(this), makingAmount);
        token.approve(_inch, makingAmount);
    }

}
